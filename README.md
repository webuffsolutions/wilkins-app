# Wilkins Mobile - Made with html/css and phonegap


## Usage

#### PhoneGap CLI

#### Step 1:

$ git clone https://bitbucket.org/webuffsolutions/wilkins-app

$ cd wilkins-app

$ npm install -g phonegap@latest

$ phonegap

#### Step 2:

Install the mobile app on your phone

(search for phonegap developer)

#### Step 3:

$ phonegap serve

Enter server address in your phonegap developer app

#### Step 4:

Click connect to preview app




